<%--
  Created by IntelliJ IDEA.
  User: 18727
  Date: 2023/11/9
  Time: 1:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册</title>
    <link rel="stylesheet" href="../../css/register.css" type="text/css"/>
</head>
<body>
<div class="box">
    <div class="content">
        <h2>用户注册</h2>
        <div>
            <input type="text" id="username" placeholder="请输入用户名"/>
        </div>
        <div>
            <input type="password" id="password" placeholder="请输入密码"/>
        </div>
        <div>
            <input type="password" id="confirmPass" placeholder="请再次输入密码"/>
        </div>
        <div>
            <input type="submit" value="注册"/>
        </div>
    </div>
</div>
</body>
</html>
