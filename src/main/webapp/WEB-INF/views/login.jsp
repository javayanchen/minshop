<%--
  Created by IntelliJ IDEA.
  User: 18727
  Date: 2023/11/8
  Time: 16:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>
    <link rel="stylesheet" href="../../css/login.css" type="text/css"/>
    <script src="../../js/jquery.js"></script>
</head>
<body>
<div class="box">
    <div class="content">
        <h2>登录</h2>
        <div>
            <input type="text" id="username" placeholder="请输入用户名"/>
        </div>
        <div>
            <input type="password" id="password" placeholder="请输入密码"/>
        </div>
        <div>
            <input type="submit" value="登录" onclick="login()"/>
        </div>
    </div>
    <a href="#" class="btns">忘记密码</a>
    <a href="/register" class="btns register">注册</a>
</div>

<script>
    function login() {
        var username = $("#username").val();
        var password = $("#password").val();

        $.ajax({
            type: "POST",
            url: "/user/login",
            data: JSON.stringify({
                username: username,
                password: password
            }),
            contentType: "application/json",
            success: function(response) {
                // TODO 处理登录成功的情况
                if (response.code === 200) {
                    alert("登录成功！");
                    // TODO 进行重定向或其他操作
                } else {
                    alert(response.message);
                }
            }
        });
    }
</script>
</body>
</html>
