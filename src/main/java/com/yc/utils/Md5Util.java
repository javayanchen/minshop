package com.yc.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @Description: 用户密码加密
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/8 5:24
 */
public class Md5Util {
    // 一个用于加密的密钥（盐值）
    private static final String SALT = "miNiShop&(A_&!@";

    // 加密方法
    public static String encrypt(String input) {
        try {
            // 将输入字符串与盐值拼接
            String saltedInput = input + SALT;
            // 创建MD5哈希对象
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 将字符串转换为字节数组，并进行哈希计算
            byte[] bytes = md.digest(saltedInput.getBytes());
            // 将字节数组转换为十六进制字符串
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "加密失败";
        }
    }

    // 解密方法（实际为验证方法）
    public static boolean verify(String input, String hashed) {
        String hashedInput = encrypt(input);
        return hashedInput.equals(hashed);
    }

}
