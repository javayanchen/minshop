package com.yc.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * @Description: 数据转换（json格式）
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/8 5:03
 */
public class JsonUtil {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String objectToJson(Object object) {
        objectMapper.registerModule(new JavaTimeModule());
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            //TODO 在后续实际应用中，你可能会考虑处理异常
            e.printStackTrace();
            return null;
        }
    }

}
