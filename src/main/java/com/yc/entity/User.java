package com.yc.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Description: 用户实体
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/2 11:46
 */
@Data
public class User {

    private int id;

    private String username;

    private String password;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    private Integer state;

}
