package com.yc.entity.dto;

import lombok.Data;

/**
 * @Description: TODO
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/8 15:27
 */
@Data
public class UserRegisterDto {

    private String username;

    private String password;

    private String confirmPass;
}
