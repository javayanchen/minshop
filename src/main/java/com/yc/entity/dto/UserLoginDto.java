package com.yc.entity.dto;

import lombok.Data;

/**
 * @Description: TODO
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/8 5:12
 */
@Data
public class UserLoginDto {

    private String username;

    private String password;

    private int errorCount;
}
