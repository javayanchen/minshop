package com.yc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description: 页面跳转
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/8 16:45
 */
@Controller
public class IndexController {

    @RequestMapping("/welcome")
    public String welcomeLogin() {
        return "login";
    }

    @RequestMapping("/register")
    public String welcomeRegister() {
        return "register";
    }


}
