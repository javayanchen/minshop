package com.yc.controller;

import com.yc.api.Result;
import com.yc.entity.dto.UserLoginDto;
import com.yc.entity.dto.UserRegisterDto;
import com.yc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: TODO
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/6 17:05
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public Result login(@RequestBody UserLoginDto userLoginDto) {

        return userService.login(userLoginDto);
    }

    @PostMapping("/register")
    public Result register(@RequestBody UserRegisterDto userRegisterDto) {

        return userService.register(userRegisterDto);
    }
}
