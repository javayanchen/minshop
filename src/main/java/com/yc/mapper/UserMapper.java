package com.yc.mapper;

import com.yc.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description: TODO
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/6 17:15
 */
@Repository
public interface UserMapper {

    List<User> queryUsers();

    //根据用户名查询用户是否存在
    User queryUser(String username);

    int addUser(User user);

}
