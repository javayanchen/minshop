package com.yc.api;

/**
 * @Description: API返回码封装类
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/8 7:14
 */
public enum ResultCode implements IErrorCode {

    SUCCESS(200, "成功"),
    FAILED(500, "失败"),
    FAILEDLOGIN(500, "登录失败"),
    VALIDATE_FAILED(404, "参数检验失败"),
    UNAUTHORIZED(401, "暂未登录或token已经过期"),
    FORBIDDEN(403, "没有相关权限");
    private long code;
    private String message;

    private ResultCode(long code, String message) {
        this.code = code;
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
