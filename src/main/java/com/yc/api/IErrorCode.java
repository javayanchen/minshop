package com.yc.api;

/**
 * @Description: API返回码接口
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/8 7:14
 */
public interface IErrorCode {

    /**
     * 返回码
     */
    long getCode();

    /**
     * 返回信息
     */
    String getMessage();
}
