package com.yc.service;

import com.yc.api.Result;
import com.yc.entity.User;
import com.yc.entity.dto.UserLoginDto;
import com.yc.entity.dto.UserRegisterDto;

import java.util.List;

/**
 * @Description: user业务接口
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/6 17:50
 */
public interface UserService {

    List<User> queryUser();

    Result login(UserLoginDto userLoginDto);

    Result register(UserRegisterDto userRegisterDto);
}
