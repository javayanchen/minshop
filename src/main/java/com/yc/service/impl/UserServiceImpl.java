package com.yc.service.impl;

import com.yc.api.Result;
import com.yc.api.ResultCode;
import com.yc.entity.User;
import com.yc.entity.dto.UserLoginDto;
import com.yc.entity.dto.UserRegisterDto;
import com.yc.mapper.UserMapper;
import com.yc.service.UserService;
import com.yc.utils.Md5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: user业务
 * @Author: YanChen
 * @Email: 1872752462@qq.com
 * @Date: 2023/11/6 17:50
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> queryUser() {
        return userMapper.queryUsers();
    }

    /**
     * 用户登录
     * @param userLoginDto
     * @return
     */
    @Override
    public Result login(UserLoginDto userLoginDto) {
        //1、判断是否存在该用户名
        User user = userMapper.queryUser(userLoginDto.getUsername());
        if (user == null) {
            //2、不存在，提示用户不存在，请先注册
            return Result.failed(ResultCode.FAILED,"用户不存在，请先注册");
        }
        //3、存在，比对用户名和密码是否一致
        String encrypt = Md5Util.encrypt(userLoginDto.getPassword());
        if (!Md5Util.verify(encrypt, user.getPassword())) {
            int errorCount = userLoginDto.getErrorCount();
            //3.1、不一致，则进行判断错误次数
            if (errorCount < 3) {
                //3.1.1、未达到3次，重新返回登录页面，并提示用户名或密码错误
                userLoginDto.setErrorCount(errorCount + 1);
                // TODO 将限制次数传递
                return Result.failed("用户名或密码错误");
            } else {
                //3.1.2、达到3次，限制登录
                return Result.failed(ResultCode.FAILED,"登录失败，请在十分钟后再尝试");
            }
        }
        //4、一致，登录成功
        return Result.success();
    }


    /**
     * 用户注册
     * @param userRegisterDto
     * @return
     */
    @Override
    public Result register(UserRegisterDto userRegisterDto) {

        // TODO 1、查询用户名是否存在


        //2、检验密码是否符合规则

        //3、校验密码和确认密码是否一致
        boolean verifyPass = userRegisterDto.getPassword().equals(userRegisterDto.getConfirmPass());

        if (!verifyPass) {
            return Result.failed("密码和确认密码不一致，请重新输入");
        }
        //3.1、密码加密&记录注册时间
        String password = Md5Util.encrypt(userRegisterDto.getPassword());
        User user = new User();
        user.setUsername(userRegisterDto.getUsername());
        user.setPassword(password);
        user.setCreateTime(LocalDateTime.now());
        try {
            userMapper.addUser(user);
        } catch (Exception e) {
            System.out.println("插入失败");
        }

        //4、注册成功
        return Result.success();
    }
}
